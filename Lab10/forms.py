from django import forms

class subscriber(forms.Form):
	form_name = {
        'type': 'email',
        'id' : 'text1',
        'placeholder':'Input Name',
        'class' : "subscribe-input",
        'required':True,
    }
	form_email = {
        'type': 'email',
        'id' : 'text2',
        'placeholder':'Email',
        'class' : "subscribe-input",
        'required':True,
    }
	form_password = {
        'type': 'password',
        'id' : 'text3',
        'placeholder':'Password',
        'class' : "subscribe-input",
        'required':True,
    }
    # email1_attrs = {
    #     'type': 'email',
    #     'id' : 'text4',
    #     'placeholder':'Email',
    #     'class' : "subscribe-input",
    #     'class': 'todo-form-input',
    #     'required':True,
    # }

	nama = forms.CharField(widget=forms.TextInput(attrs=form_name), max_length=50)
	email = forms.EmailField(widget=forms.EmailInput(attrs=form_email))
	password = forms.CharField(widget=forms.PasswordInput(attrs=form_password), min_length=8)
    # email1 = forms.EmailField(widget=forms.EmailInput(attrs=email1_attrs))