from django.urls import path
from .views import *

urlpatterns = [
	path('', formReg, name="form"),
	path('addSubscribe/', addSub, name="addSubscribe"),
	path('validate', validate, name="validate"),
	path('unSubscribe', deleteObject, name="unSubscribe")

]
