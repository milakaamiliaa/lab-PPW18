from django.shortcuts import render, redirect
from django.http import HttpResponseRedirect, HttpResponse, JsonResponse
from django.template import RequestContext
from django.views.decorators.csrf import csrf_exempt
from .models import Subscribe
from .forms import subscriber
import json
from django.core import serializers

# Create your views here.
response = {}

def formReg(request):
	form = subscriber(request.POST or None)
	return render(request, "subscribe.html", {'form':form})

@csrf_exempt
def addSub(request):
	if (request.method == 'POST'):
		enctype = "multipart/form-data"
		nama = request.POST['nama']
		email = request.POST['email']
		password = request.POST['password']

		subscriber = Subscribe(nama = nama, email = email, password = password)
		subscriber.save() #ngesave data ke models dulu
		data = getObject(subscriber)
		return HttpResponse(data) # ngirim data ke jengo
	else:
		sub = Subscribe.objects.all().values()
		data = list(sub)
		return JsonResponse(data, safe = False)

@csrf_exempt
def validate(request):
	enctype = "multipart/form-data"
	email = request.POST.get('email')
	data = {
		'is_taken':Subscribe.objects.filter(email=email).exists()
		}
	return JsonResponse(data)

def getObject(obj):
	data = serializers.serialize('json', [obj,])
	struct = json.loads(data)
	data = json.dumps(struct[0]["fields"])
	return data

def deleteObject(request):
	if request.method == "POST":
		email = request.POST['email']
		Subscribe.objects.filter(email = email).delete()
		return HttpResponseRedirect(json.dumps({'statusSub': "Success"}), content_type="application/json")










