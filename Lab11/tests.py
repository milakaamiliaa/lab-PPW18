
from django.test import TestCase
from django.test import Client
from django.urls import resolve
from .views import login, book_table, get_Json
from django.apps import apps
from .apps import Lab11Config

class Lab11Test(TestCase):
    
    def test_story11_index_func(self):
        found = resolve('')
        self.assertEqual(found.func, login)

    def test_story11_index_func(self):
        found = resolve('/')
        self.assertEqual(found.func, book_table)    
    
    # def test_template_story11(self):
    #     response = Client().get('/login')
    #     self.assertTemplateUsed(response, 'login.html')

    # def test_template_story11(self):
    #     response = Client().get('/')
    #     self.assertTemplateUsed(response, 'book_table.html')

    
    