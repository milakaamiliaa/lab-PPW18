from django.conf.urls import url, include
from django.contrib import admin
###from django.contrib.auth import views
from django.urls import path
from .views import *


urlpatterns = [
    path('admin', admin.site.urls),
    path('login', login, name='login'),
    path('', book_table, name = 'book_table'),
    path('logout', logout, name='logout'),
    url(r'^auth/', include('social_django.urls', namespace='social')),
]