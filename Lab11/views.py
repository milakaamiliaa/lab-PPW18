from django.shortcuts import render

# Create your views here.
#buat login, logout,
from django.http import HttpResponseRedirect,HttpResponse
from django.http import JsonResponse
from django.views.decorators.csrf import csrf_exempt
import json
from django.shortcuts import render
import requests
from django.contrib.auth import logout
response = {}

#tried reinstalling social django
def login(request):
	return render(request, 'login.html')

def book_table(request):
	# if request.user.is_authenticated:
		# request.session['user'] = request.user.username
		# request.session['email'] = request.user.email
		# count = request.session.get('count', 0)
		# print(dict(request.session))
		# for key, value in request.session.items():
		# 	print('{} => {}'.format(key, value))
    return render(request,'book_table.html',response)

def logout(request):
	request.session.flush()
	logout(request)
	return HttpResponseRedirect('/')

@csrf_exempt
def get_Json(request):
    get_the_json = requests.get('https://www.googleapis.com/books/v1/volumes?q=quilting')
    the_books = (get_the_json.json())['items']
    actual_json = json.dumps({'items': the_books})
    return HttpResponse(actual_json,content_type="application/json")

def addFav(request):
	print(dict(request.session))
	request.session['count'] = request.session['count'] + 1
	return HttpResponse(request.session['count'], content_type = 'application/json')

def removeFav(request):
	request.session['count'] = request.session['count'] - 1
	return HttpResponse(request.session['count'], content_type = 'application/json')
