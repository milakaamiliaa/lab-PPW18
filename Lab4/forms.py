from django import forms

class Content(forms.Form):
    atr = {
            'class' : 'form-control'
    }

    activity = forms.CharField(label="activity", required=True)
    location = forms.CharField(label="location", required=True)
    category = forms.CharField(label="category", required=True)
    date = forms.DateTimeField(required=True, widget=forms.DateTimeInput(attrs={'type': 'datetime-local'}), input_formats=['%Y-%d-%mT%H:%M'])