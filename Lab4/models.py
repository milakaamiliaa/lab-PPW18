from django.db import models
from django.utils import timezone
from datetime import datetime, date

class Message(models.Model):
    activity = models.CharField(max_length=27)
    location = models.CharField(max_length=27)
    category = models.CharField(max_length=27)
    date = models.DateTimeField(auto_now_add=True)