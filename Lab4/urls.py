from django.urls import path
from . import views
from .views import scheduleContent

urlpatterns = [
    path('', views.homepage, name = 'home'),
	path('Homepage/', views.homepage, name = 'home'),
	path('Education/', views.education, name = 'education'),
	path('Experience/', views.experience, name = 'experience'),
	path('Contact/', views.contact, name = 'contact'),
	path('Register/', views.register, name = 'register'),
	path('Skills/', views.skills, name = 'skills'),
    path('Schedule/', views.scheduleContent, name = 'schedule'),
    path('Post/', views.post, name = 'post'),
    path('showSched/', views.showSched, name = 'showSched'),
    path('showSched/deleteact/', views.deleteact, name = 'deleteact')
]
