from django.shortcuts import render
from .forms import Content
from .models import Message
from django.http import HttpResponseRedirect

#import models

response = {'author' : 'milakaamiliaa'}

# Create your views here.
def homepage(request):
    return render(request, 'Homepage.html')

def education(request):
    return render(request, 'Education.html')

def experience(request):
    return render(request, 'Experience.html')

def contact(request):
    return render(request, 'Contact.html')

def register(request):
    return render(request, 'Register.html')

def skills(request):
    return render(request, 'Skills.html')

def scheduleContent(request):
    response = {'form' : Content}
    return render(request, 'Schedule.html', response)

def post(request):
    form = Content(request.POST or None)
    print(form.errors)
    if(request.method == 'POST'):
        print("MASUK SINI")
        response['activity'] = request.POST['activity']
        response['location'] = request.POST['location']
        response['category'] = request.POST['category']
        response['date'] = request.POST['date']
        sched = Message(activity = response['activity'],
                      category = response['category'],
                      location = response['location'],
                      date = response['date'])
        sched.save()
        return HttpResponseRedirect('/Schedule/')
    else:
        return HttpResponseRedirect('/Schedule/')

def showSched(request):
    sched = Message.objects.all()
    # response = {'sched':sched}
    return render(request, 'schedUpdate.html', {'sched':sched})

def deleteact(request):
    sched = Message.objects.all().delete()
    response['sched'] = sched
    return render(request, 'schedUpdate.html',response)
