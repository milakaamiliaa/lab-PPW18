from django.db import models
from django.utils import timezone
from datetime import datetime, date

class Post(models.Model):
   content = models.CharField(max_length=300)
   time = models.DateTimeField(default=timezone.now)