from django.urls import path
from . import views
from .views import homepage, book_table, get_Json

urlpatterns = [
    path('',views.book_table, name = 'book'),
    # path('getjson/',get_Json,name='getjson'),
    path('getjson/<str:keyword>', get_Json,name='get_json')
]
