from . import views
from django.shortcuts import render, redirect
from django.http import HttpResponseRedirect,HttpResponse
from django.http import JsonResponse
from django.views.decorators.csrf import csrf_exempt
import json
import requests

response ={}

def homepage(request):
    return render(request, 'homepage.html')

def book_table(request):
    return render(request,'book_table.html',response)

@csrf_exempt
def get_Json(request, keyword='quilting'):
    get_the_json = requests.get('https://www.googleapis.com/books/v1/volumes?q=quilting'+keyword)
    the_books = (get_the_json.json())['items']
    actual_json = json.dumps({'items': the_books})
    return HttpResponse(actual_json,content_type="application/json")
