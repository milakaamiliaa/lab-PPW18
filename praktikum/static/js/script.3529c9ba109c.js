// ACCORDION
$( function() {
  $( "#accordion" ).accordion();({
  collapsible: true
  });
} );

function change(){
  var classAtr = document.getElementById('body')
  if(classAtr.className == 'w3-black'){
    document.getElementById('body').className = 'w3-white'
    document.getElementById('name').className = 'w3-text-grey'
    document.getElementById('skills').className = 'w3-text-grey'
    document.getElementById('contactme').className = 'w3-text-grey'
  }else {
    document.getElementById('body').className = 'w3-black'
  }
}

// function loadBar() {
//   var myVar;
//   console.log("apa")
//   myVar = setTimeout(showPage, 100);
// }

function showPage() {
  document.getElementById("loader").style.display = "none";
  document.getElementById("myDiv").style.display = "block";
}

// function changeJSON() {
//   var obj = {[{"id":"158568","name":"[Bundling] Intel Core i5-8600K 3.6Ghz Up To 4.3Ghz - Cache 9MB [Box] Socket LGA 1151 - Coffeelake Series + XFX Radeon RX 580 8GB DDR5 GTS XXX OC+ Dual Fan - RX-580P8DFD6","details":"","brand":"298","category":"108","subcategory":"","brand_description":"Others","category_description":"bundle","subcategory_description":"","price":"8490000","weight":"2500","quantity":"0","stock_type":"0","link_toped":null,"link_shopee":null,"link_bukalapak":null},{"id":"158567","name":"[Bundling] Intel Core i5-8600K 3.6Ghz Up To 4.3Ghz - Cache 9MB [Box] Socket LGA 1151 - Coffeelake Series + XFX Radeon RX 570 8GB DDR5 RS XXX Edition - RX-570P8DFD6","details":"","brand":"298","category":"108","subcategory":"","brand_description":"Others","category_description":"bundle","subcategory_description":"","price":"7950000","weight":"2500","quantity":"0","stock_type":"0","link_toped":null,"link_shopee":null,"link_bukalapak":null}]};
//   var obj_JSON = JSON.stringify(obj);
//   window.location = 'link apa YANG ADA DISINI' + obj_JSON;

// }

var favorites = 0;
/**Implementasi supaya bintangnya bisa nyala */
function star(the_id){
    var star_url_biru = "https://i.imgur.com/7CeVmZu.png";
    var star_url_kuning = "https://i.imgur.com/SPTSn8Y.png";;
   
    if(document.getElementById(the_id).src==star_url_biru){
        //console.log('success',the_id);
        favorites+=1;
        document.getElementById(the_id).src=star_url_kuning;
    }
    else{
       // console.log('succes',document.getElementById(the_id).src);
        if(favorites>0){
            favorites-=1;
        }
        else{
            favorites=0;
        }
        document.getElementById(the_id).src=star_url_biru;
    }
    document.getElementById("fav").innerHTML = "Favorites: "+favorites;
}


$(document).ready(function(){
    $('[data-toggle="tooltip"]').tooltip();   
    $(".accordion").on("click", ".accordion-header", function() {
        $(this).toggleClass("active").next().slideToggle();
    });
    /*Get Json dan menampilkan Json pada tabel */
    $.ajax({
        type:'GET',
        dataType: 'jsonp',
        url: 'https://www.googleapis.com/books/v1/volumes?q=quilting', 
        crossDomain: true,
        success: function(data){
           // console.log('success',data);
            var html_string = "";
            var unique_id;
            $.each(data.items,function(i,book){
                unique_id=""+book.volumeInfo.industryIdentifiers[1].identifier;
                if (unique_id.startsWith("0")){
                    unique_id = unique_id.slice(1,unique_id.length);
                  //  console.log('id: ',unique_id);
                }
                if (unique_id.endsWith("X")){
                    unique_id= unique_id.slice(0,unique_id.length-1);
                    //console.log('id: ',unique_id);
                }
                var star_url = "https://i.imgur.com/7CeVmZu.png";
                html_string+= "<tr>"+
                '<td class = "image">' + "<img src='"+data.items[i].volumeInfo.imageLinks.smallThumbnail +"'></img>" + "</td>"+
                "<td>"+book.volumeInfo.title+"</td>"+
                 "<td>"+book.volumeInfo.authors+"</td>"+
                 "<td>"+book.volumeInfo.publisher+"</td>"+
                 "<td>"+book.volumeInfo.publishedDate+"</td>"+
                 " <td><img src=\""+star_url+"\" id="+unique_id+" onclick=\"star("+unique_id+")\" style =\"max-width:20px;max-height:20px;\"></td>"+
                 "</tr>";
            })
            $("#start").append(html_string)
        }

    });
});

