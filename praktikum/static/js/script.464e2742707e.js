// ACCORDION
$( function() {
  $( "#accordion" ).accordion();({
  collapsible: true
  });
} );

function change(){
  var classAtr = document.getElementById('body')
  if(classAtr.className == 'w3-black'){
    document.getElementById('body').className = 'w3-white'
    document.getElementById('name').className = 'w3-text-grey'
    document.getElementById('skills').className = 'w3-text-grey'
    document.getElementById('contactme').className = 'w3-text-grey'
  }else {
    document.getElementById('body').className = 'w3-black'
  }
}

// function showPage() {
//   document.getElementById("loader").style.display = "none";
//   document.getElementById("myDiv").style.display = "block";
// }

var favorites = 0;
/**Implementasi supaya bintangnya bisa nyala */
function star(the_id){
    var star_url_biru = "https://i.imgur.com/7CeVmZu.png";
    var star_url_kuning = "https://i.imgur.com/SPTSn8Y.png";;
   
    if(document.getElementById(the_id).src==star_url_biru){
        //console.log('success',the_id);
        favorites+=1;
        document.getElementById(the_id).src=star_url_kuning;
    }
    else{
       // console.log('succes',document.getElementById(the_id).src);
        if(favorites>0){
            favorites-=1;
        }
        else{
            favorites=0;
        }
        document.getElementById(the_id).src=star_url_biru;
    }
    document.getElementById("fav").innerHTML = "Favorites: "+favorites;
}


$(document).ready(function(){
    $('[data-toggle="tooltip"]').tooltip();   
    $(".accordion").on("click", ".accordion-header", function() {
        $(this).toggleClass("active").next().slideToggle();
    });
    /*Get Json dan menampilkan Json pada tabel */
    $.ajax({
        type:'GET',
        dataType: 'json',
        url: 'https://www.googleapis.com/books/v1/volumes?q=quilting', 
        crossDomain: true,
        success: function(data){
           // console.log('success',data);
            var html_string = "";
            var unique_id;
            $.each(data.items,function(i,book){
                unique_id=""+book.volumeInfo.industryIdentifiers[1].identifier;
                if (unique_id.startsWith("0")){
                    unique_id = unique_id.slice(1,unique_id.length);
                  //  console.log('id: ',unique_id);
                }
                if (unique_id.endsWith("X")){
                    unique_id= unique_id.slice(0,unique_id.length-1);
                    //console.log('id: ',unique_id);
                }
                var star_url = "https://i.imgur.com/7CeVmZu.png";
                html_string+= "<tr>"+
                '<td class = "image">' + "<img src='"+data.items[i].volumeInfo.imageLinks.smallThumbnail +"'></img>" + "</td>"+
                "<td>"+book.volumeInfo.title+"</td>"+
                 "<td>"+book.volumeInfo.authors+"</td>"+
                 "<td>"+book.volumeInfo.publisher+"</td>"+
                 "<td>"+book.volumeInfo.publishedDate+"</td>"+
                 " <td><img src=\""+star_url+"\" id="+unique_id+" onclick=\"star("+unique_id+")\" style =\"min-width:40px;min-height:40px;\"></td>"+
                 "</tr>";
            })
            $("#start").append(html_string)
        }

    });
});

