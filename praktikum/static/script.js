var favorites = 0;
/**Implementasi supaya bintangnya bisa nyala */
function star(the_id){
    var star_url_biru = "https://i.imgur.com/7CeVmZu.png";
    var star_url_kuning = "https://i.imgur.com/SPTSn8Y.png";;
   
    if(document.getElementById(the_id).src==star_url_biru){
        favorites+=1;
        document.getElementById(the_id).src=star_url_kuning;
    }
    else{
        if(favorites>0){
            favorites-=1;
        }
        else{
            favorites=0;
        }
        document.getElementById(the_id).src=star_url_biru;
    }
    document.getElementById("fav").innerHTML = "Favorites: "+favorites;
}

var logout = function() {
    document.location.href = "https://www.google.com/accounts/Logout?continue=https://appengine.google.com/_ah/logout?continue=http://http://127.0.0.1:8000/login";
}


$(document).ready(function(){
    var html_string = "";
    var keyword ="quilting";
    var urls = 'https://www.googleapis.com/books/v1/volumes?q=quilting';
    var counter = 0;
    $('[data-toggle="tooltip"]').tooltip();   
    $(".accordion").on("click", ".accordion-header", function() {
        $(this).toggleClass("active").next().slideToggle();
    });

    $('#search_button').click(function(){
        keyword = $('#search_box').val();
        if (keyword.length!=0){
            html_string = "";
        }
        $.ajax({
            type: 'GET',
            dataType: 'json',
            url:'/getjson/'+keyword,
            crossDomain:true,
            success: function(data){
            console.log(keyword);
                
            var unique_id;
            $.each(data.items,function(i,book){
                unique_id=""+counter;
                counter+=1;
                var star_url = "https://i.imgur.com/7CeVmZu.png";
                html_string+= "<tr>"+
                '<td class = "image">' + "<img src='"+data.items[i].volumeInfo.imageLinks.smallThumbnail +"'></img>" + "</td>"+
                "<td>"+book.volumeInfo.title+"</td>"+
                 "<td>"+book.volumeInfo.authors+"</td>"+
                 "<td>"+book.volumeInfo.publisher+"</td>"+
                 "<td>"+book.volumeInfo.publishedDate+"</td>"+
                 " <td><img src=\""+star_url+"\" id="+unique_id+" onclick=\"star("+unique_id+")\" style =\"max-width:20px;max-height:20px;\"></td>"+
                 "</tr>";
            })
            $("#start").html(html_string)

            }
        });

    });

    /*Get Json dan menampilkan Json pada tabel */
    $.ajax({
        type:'GET',
        dataType: 'json',
        url: 'https://www.googleapis.com/books/v1/volumes?q=quilting', 
        crossDomain: true,
        success: function(data){
           // console.log('success',data);
            var html_string = "";
            var unique_id;
            $.each(data.items,function(i,book){
                unique_id=""+counter;
                counter += 1;
                // if (unique_id.startsWith("0")){
                //     unique_id = unique_id.slice(1,unique_id.length);
                //   //  console.log('id: ',unique_id);
                // }
                // if (unique_id.endsWith("X")){
                //     unique_id= unique_id.slice(0,unique_id.length-1);
                //     //console.log('id: ',unique_id);
                // }
                var star_url = "https://i.imgur.com/7CeVmZu.png";
                html_string+= "<tr>"+
                '<td class = "image">' + "<img src='"+data.items[i].volumeInfo.imageLinks.smallThumbnail +"'></img>" + "</td>"+
                "<td>"+book.volumeInfo.title+"</td>"+
                 "<td>"+book.volumeInfo.authors+"</td>"+
                 "<td>"+book.volumeInfo.publisher+"</td>"+
                 "<td>"+book.volumeInfo.publishedDate+"</td>"+
                 " <td><img src=\""+star_url+"\" id="+unique_id+" onclick=\"star("+unique_id+")\" style =\"min-width:40px;min-height:40px;\"></td>"+
                 "</tr>";
            })
            $("#start").append(html_string)
        }

    });
});

